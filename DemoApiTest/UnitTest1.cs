using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DemoApiTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [DataRow("DemoApi", true)]
        [DataRow("Api", false)]
        [DataRow("", false)]
        public void ValidateTest(string name,bool expected)
        {
            Assert.AreEqual(expected, DemoApi.Core.Logic.validateText(name));
        }
    }
}

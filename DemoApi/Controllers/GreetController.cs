﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace DemoApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GreetController : ControllerBase
    {
        private readonly ILogger<GreetController> _logger;

        public GreetController(ILogger<GreetController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{name}")]
        public ActionResult Get(string name)
        {
            if (Core.Logic.validateText(name))
                return StatusCode(StatusCodes.Status200OK, $"Greetings {name}! from {Environment.MachineName} at {DateTime.Now}");
            else
                return StatusCode(StatusCodes.Status400BadRequest, "Unknown not allowed");
        }
    }
}
